---
layout: page
title: Tentang Kami
permalink: /tentang-kami/
image: '/assets/img/08.jpg'
---

Media pembelajaran ini kami susun dalam rangka memenuhi tugas besar media pembelajaran untuk Siswa Sekolah Menengah Atas (SMA) pada Mata Kuliah Psikologi Pendidikan Kelas 3. Yang diampu oleh Ibu Dinni Asih Febriyanti, S.Psi., M.Psi.

***

Media pembelajaran ini disusun oleh Kelompok 5 yang beranggotakan,

* Faidzin Nur Hidayat	(15000119140265)
* Imam Nurcholis  	(15000119130314)
* Syaidul Akbar		(15000119130156)
* Farika Agni Hotri	(15000119120065)
* Audrey Rizky Pasha	(15000119140162)

***

Semoga media pembelajaran ini bermanfaat bagi siapapun yang menggunakannya.
