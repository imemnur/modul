---
layout: post
title:  Kenapa Merkantilisme Bisa Muncul? Apa Penyebabnya?
date:   2020-11-09 14:50:35 +0300
tags:   [materi, kelas 11, sejarah, rempah-rempah, eropa]
---

Layaknya sebuah peribahasa "tidak akan ada asap kalau tidak ada api." Kemunculan merkantilisme pasti memiliki sebab dan latar belakangnya nih teman-teman. Lalu apa sih latar belakang dari munculnya merkantilisme itu? untuk mengetahuinya ayo simak baik-baik materi berikut.

---
Pada masa renaisans (abad pencerahan) banyak negara besar Eropa yang berusaha memperkuat kondisi perekonomiannya. Dengan perekonomian yang kuat, maka diharapkan dapat tercipta kemakmuran dan kesejahteraan untuk rakyat Eropa. Untuk mencapai tujuan tersebut, maka digunakanlah sistem ekonomi merkantilisme. Selain keinginan untuk memperkuat ekonomi dan mensejahterakan rakyat, ada beberapa faktor lain yang menyebabkan munculnya merkantilisme, faktor tersebut diantaranya sebagai-berikut.

* Adanya keberadaan negara-negara merdeka di Eropa seperti Prancis, Jerman, Inggirs, Belanda, dan Italia.
* Negara-negara tersebut memerlukan kondisi ekonomi yang kuat agar bisa bertahan.
* Ditetapkannya logam mulia sebagai standar ukuran kekayaan suatu negara.
* Adanya jaringan perdagangan, pelayaran, dan eksplorasi ke wilayah-wilayah baru.

---
Nah sekarang coba deh teman-teman diskusikan sama teman kalian atau kalian juga bisa mencari di internet dan buku sejarah. Selain faktor yang sudah disebutkan tadi, masih ada ngga sih faktor lain yang menyebabkan merkantilisme?. Kalau ada boleh banget nih tulis jawaban kalian di kolom komentar, aku tunggu ya jawabannya!.

---
jadi gimana teman-teman? sekarang udah bisa jawab kan kalau diminta menyebutkan faktor terjadinya merkantilisme? selanjutnya, kalian pasti penasaran siapa sih tokoh-tokoh yang berpengaruh pada merkantilisme, untuk mengetahuinnya langsung aja yuk kita ke materi selanjutnya!.

<div class="author__more">
  <a href="{{site.baseurl}}/2020/11/11/materi-3/" class="say-hello">Lanjut ke materi selanjutnya</a>
</div>