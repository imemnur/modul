---
layout: post
title:  Tokoh Besar dalam Merkantilisme
date:   2020-11-11 14:50:35 +0300
tags:   [materi, kelas 11, sejarah, rempah-rempah, eropa]
---

Seperti yang kita ketahui, setiap kejadian besar yang ada di dalam sejarah pasti memunculkan beberapa nama besar. Contohnya saja, dalam sejarah proklamasi Indonesia terdapat beberapa tokoh besar seperti Ir. Soekarno, Moh. Hatta, dan Sayuti Melik yang namanya terus diingat hingga masa sekarang. Nah, merkantilisme juga memiliki beberapa tokoh besar nih teman-teman, penasaran kan siapa saja tokohnya? langsung aja yuk cek materinya!.

---

Suatu pemikiran atau aliran tidak akan ada, jika tidak ada yang mencetuskan, mempercayai, dan mengikutinya. Begitu pula dengan merkantilisme, suatu paham yang cukup signifikan menyumbang perubahan pada dunia pada abad pertengahan, terutama dunia eropa. Bagaimana tidak? Dari pemikiran ini, dunia mulai mengenal paham perdagangan keluar, atau ekspor dan impor. Aliran ini selain berpengaruh pada sejarah perkembangan dunia, terutama eropa, pasca the dark age, aliran ini juga mempengaruhi pemikiran ekonomi dunia pada saat itu. Berikut adalah tokoh-tokoh pada Merkantilisme.

## 1. Thomas Mun
![]({{site.baseurl}}/assets/img/thomas.png)

Thomas Mun dapat dikatakan menjadi salah satu tokoh pencetus aliran ini. Pada bukunya yang berjudul England Treasure by Foreign Trade, ia menggagas opini pentingnya perdagangan luar negeri bagi suatu negara. Ia berpendapat perdagangan dalam negeri tidak membuat negara lebih makmur, karena pertukaran yang terjadi tidak berpengaruh besar pada kekayaan negara. Negara perlu membuka perdagangan keluar untuk meraup emas dan perak yang lebih banyak.

## 2. Jean Baptis Colbert
![]({{site.baseurl}}/assets/img/jean.png)

Jean Baptis Colbert selaku pejabat Negara Perancis dengan kedudukan sebagai Menteri Utama di Bidang Ekonomi dan keuangan dalam pemerintahan Louis XIV, merupakan salah satu tokoh di dunia yang menyumbang pada berkembangnya merkantilisme di eropa. Ia mengeluarkan kebijakan-kebijakan yang memudahkan pengrajin-pengrajin serta pedagang-pedagan local. Ia juga membuat kebijakan-kebijakan di sector ekonomi yang mendorong pengembangan ilmu pengetahuan agar muncul inovasi-inovasi terbaru.

## 3. Sir Dudley North
![]({{site.baseurl}}/assets/img/dudley.png)

North juga merupakan salah satu tokoh yang cukup vocal dalam menggaungkan pentingnya perdagangan luar negeri. North beranggapan bahwa emas bukan merupakan symbol dari kekayaan negara, tapi menjadi alat tukar untuk memajukan perdagangan. North juga sangat mendukung penggunaan emas asalkan untuk keperluan perdagangan

## 4. Perdana Menteri Cromwell
![]({{site.baseurl}}/assets/img/cromwell.png)

Sama seperti Colbert, Cromwell juga merupakan pejabat negara Inggris, tepatnya Perdana Menteri Inggris pada masa pemerintahan Ratu Elizabeth I. Cromwell juga mengeluarkan kebijakan-kebijakan yang melindungi usaha-usaha rakyat, serta ia juga mengeluarkan undang-undang pelayaran yang disebut Act of Navigation yang berisi bahwa semua kegiatan ekspor impor dari dan ke inggris harus menggunakan kapal inggris.

---
Nah, sekarang udah tau kan beberapa tokoh besar yang ada dalam sejarah merkantilisme. Sebenarnya masih banyak tokoh besar lain yang ada dalam sejarah merkantilisme, jika kalian masih kepo dan ingin tau siapa saja sih tokoh lain itu, boleh banget nih kalian cari informasinya di buku atau internet. Kalau udah ketemu dan kalian ingin berbagi dengan teman kalian yang lain, bisa banget informasi tersebut kalian tulis di kolom komentar.

---
Sekarang kita lanjut yuk ke materi selanjutnya.

<div class="author__more">
  <a href="{{site.baseurl}}/2020/11/11/materi-5/" class="say-hello">Lanjut ke materi selanjutnya</a>
</div>
